//import dr library besar React
import React, {useState, useEffect} from 'react';
import {ScrollView, View} from 'react-native';

//import SampleComponent.js
import SampleComponent from './pages/SampleComponent';

//import MateriFlexBox.js
import FlexBox from './pages/FlexBox';

//PositionReactNative.js
import Position from './pages/Position';

//import StylingReactNativeComponent.js
import StylingComponent from './pages/StylingComponent';

import PropsDinamis from './pages/PropsDinamis';
import StateDinamis from './pages/StateDinamis';
import Communication from './pages/Communication';
import ReactNativeSVG from './pages/ReactNativeSVG';
import BasicJavascript from './pages/BasicJavascript';

const App = () => {
  const [isShow, setIsShow] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      setIsShow(false);
    }, 6000);
  }, []);

  return (
    <View>
      <ScrollView>
        {/* <SampleComponent /> */}
        {/* <StylingComponent /> */}
        {/* <FlexBox /> */}
        {/* <Position />  */}
        {/* <PropsDinamis /> */}
        {/* <StateDinamis /> */}
        {/* <Communication /> */}
        <BasicJavascript />
        {/* <ReactNativeSVG /> */}
      </ScrollView>
    </View>
  );
};

export default App;
