import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const BasicJavascript = () => {
  // 1 jeruk + 1 jeruk = 2 jeruk;
  // 1 j + 1 j = 2 j;

  // Tipe Data

  // Primitive
  const nama = 'Fauzan Athallah'; //String
  let usia = 24; //number
  const apakahLakiLaki = true; //boolean

  // Complex
  const hewanPeliharaan = {
    nama: 'Miaw',
    jenis: 'Kucing',
    usia: 2,
    warna: 'Kuning',
    orangTua: {
      jantan: 'Kaisar',
      betina: 'Queen',
    },
  }; // Object

  const sapaOrang = (name, age) => {
    return console.log('Hello ${name} usia anda ${age}');
  }; // Function
  sapaOrang('Fauzan', 21);

  const namaOrang = ['fauzan', 'Ojan', 'ozank', 'onank', 'kakak']; //object
  typeof namaOrang; // object

  //jika hewan peliharaan bernama miaw, maka hallo miaw. Tetapi jika bukan Miaw, maka hewan siap nih
  // if(hewanPeliharaan.nama === 'Miaw') {
  //     console.log('Hallo Miaw')
  // } else {
  //     console.log('Hewan Siapa nih?')
  // }

  //if else
  const sapaHewan = objectHewan => {
    // let hasilSapa = '';
    // if(objectHewan.nama === 'Miaw') {
    //     hasilSapa = 'Hallo Miaw apa kabar ?'
    // } else {
    //     hasilSapa = 'Ini hewan siapa ?'
    // }
    // return hasilSapa

    return objectHewan.nama === 'Miaw'
      ? 'Hallo Miaw apa kabar?'
      : 'ini hewan siapa ?';
  };

  return (
    <View style={styles.container}>
      <Text style={styles.textTitle}>
        Materi Basic Javascript di React Native
      </Text>

      <Text>Name: {nama}</Text>
      <Text>Age: {usia}</Text>

      <Text>{sapaHewan(hewanPeliharaan)}</Text>
      <Text>{namaOrang[0]}</Text>
      <Text>{namaOrang[1]}</Text>
      <Text>{namaOrang[2]}</Text>
      <Text>=======</Text>
      <Text>Looping</Text>
      {namaOrang.map(orang => (
        <Text>{orang}</Text>
      ))}
    </View>
  );
};

export default BasicJavascript;

const styles = StyleSheet.create({
  container: {padding: 20},
  textTitle: {textAlign: 'center'},
});
