import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import macpro from '../../assets/image/macpro.jpg';

const Position = () => {
return (
    <View style={styles.wrapper}>
        <Text>Materi Position</Text>
        <View style={styles.macWrapper}>
        <Image source={macpro} style={styles.iconMac} />
        <Text style={styles.notif}>10</Text>
    </View>
    <Text style={styles.text}>Keranjang Belanja Anda</Text>
    </View>
);
};

const styles = StyleSheet.create({
    wrapper: {padding: 20, alignItems: 'center'},
    macWrapper: {
        borderWidth: 1, 
        borderColor: '#4398D1', 
        width:93, 
        height: 93, 
        borderRadius: 93 / 2, 
        justifyContent: 'center', 
        alignItems: 'center',
        position: 'relative',
        marginTop: 40,
    },
    iconMac: {width: 90, height: 90, borderRadius: 45},
    text: {
        fontSize: 12, 
        color: '#777777', 
        fontWeight: '700', 
        marginTop: 8,
    },
    notif: {
        fontSize: 12, 
        color: 'white', 
        backgroundColor: '#6fcf97', 
        padding: 4, 
        paddingLeft: 5,
        borderRadius: 25,
        width: 24, 
        height: 24, 
        position: 'absolute',
        top: 0,
        right: 0,
    },
});

export default Position;
