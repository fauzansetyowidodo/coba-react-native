import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import FileFoto from '../../assets/image/filefoto.svg'

const ReactNativeSVG = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.textTitle}>Materi Menggunakan file SVG didalam React Native</Text>
            <FileFoto width={244} height={125}/>
        </View>
    )
}

export default ReactNativeSVG

const styles = StyleSheet.create({
    container: {padding: 20},
    textTitle: {textAlign: 'center'},
})
